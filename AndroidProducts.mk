#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_scorch.mk

COMMON_LUNCH_CHOICES := \
    lineage_scorch-user \
    lineage_scorch-userdebug \
    lineage_scorch-eng
